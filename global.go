package log

import (
  "os"
  "bufio"
)

// The global logger. All static functions will use this
// logger. Anything going to stdout will be passed to this
// logger as INFO. If this is set to nil, then stdout will
// be restored to it's initial state. If this is set back
// to a valid logger, stdout will not be captured again.
//
// By default, this is a *Logger. But, this may be overwritten
// with any LeveledLogger you choose. So, it is not always
// safe to cast it to a *Logger.
var GlobalLogger LeveledLogger

// The old stdout. Will be used to restore stdout if you set
// GlobalLogger to nil. Can also be used to create a new logger
// that will overwrite the current GlobalLogger. You can also
// use this to print messages directly to stdout, but I don't
// know why you would want to do that.
var OldStdout *os.File

func init() {
  // global_log prints to the real stdout.
  l := New(os.Stdout)
  l.SetOutputDir("log")
  GlobalLogger = l
  // Create a pipe to pipe data to stdout.
  r, w, err := os.Pipe()
  if err != nil { panic(err) }
  // Now anything going to stdout will
  // be passed into global_log.
  OldStdout = os.Stdout
  os.Stdout = w
  // Capture stdout
  go func() {
    in := bufio.NewReader(r)
    for {
      line, err := in.ReadString('\n')
      if GlobalLogger == nil {
        OldStdout.Write([]byte(line))
        break
      }
      // GlobalLogger will do nothing if it is nil,
      // so it is fine not to have a lock around it.
      if err != nil {
        GlobalLogger.Error("Could not data going to stdout:", err)
        break
      } else {
        GlobalLogger.Info(line)
      }
    }
    // Restore stdout. This may break
    // other tools which use Pipe() and
    // stdout, but I can't imagine that
    // being an issue.
    os.Stdout = OldStdout
  }()
}


// Logs a debug message using the global logger.
func Debug(value ...interface{}) { GlobalLogger.Debug(value...) }
// Logs info using the global logger.
func Info(value ...interface{}) { GlobalLogger.Info(value...) }
// Logs a warning using the global logger.
func Warn(value ...interface{}) { GlobalLogger.Warn(value...) }
// Logs an error using the global logger.
func Error(value ...interface{}) { GlobalLogger.Error(value...) }
