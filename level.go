package log

// This is a log level. Represents how
// much verbosity a logger should print.
type Level int

// These are the possible log levels.
// NONE should always be last.
const (
  DEBUG Level = iota
  INFO
  WARN
  ERROR
  NONE
)

// Returns the log level as a string. Will
// be one of DEBUG, INFO, etc.
func (l Level) String() string {
  switch l {
    case DEBUG: return "DEBUG"
    case INFO:  return "INFO"
    case WARN:  return "WARN"
    case ERROR: return "ERROR"
    default:    return "NONE"
  }
}

// Returns the log level as a short string.
// This is what is actually used in the log
// messages. Will be one of D, I, W, etc.
func (l Level) ShortString() string {
  switch l {
    case DEBUG: return "D"
    case INFO:  return "I"
    case WARN:  return "W"
    case ERROR: return "E"
    default:    return ""
  }
}
