package log_test

import (
  "sync"
  "testing"

  "gitlab.com/macmv/log"
)

// This benchmark isn't very useful. It's mostly just to make sure nothing is using up
// a bunch of time. I'm writing the entire log message into a bytes.Buffer, and then
// writing that to stdout. It turns out stdout is very slow, and that takes up a large
// majority of the time.
//
// With a faster stdout (not an ssh session) it turns out that most of the time is spent
// on fmt.doPrintf(). This means there is basically nothing to optimize, as I would like
// to keep the default fmt printing settings.
//
// This also serves as example code. I overwrite the global logger, change the log level,
// and disable colors in this block.
func BenchmarkManyLog(b *testing.B) {
  for i := 0; i < b.N; i++ {
    var wg sync.WaitGroup
    // Want to use a custom logger, with custom settings.
    // If you want to do this, use log.OldStdout as the
    // output.
    l := log.New(log.OldStdout)
    // Set the logger level like this
    l.Level = log.WARN
    // Disable colors with this
    l.ColorsEnabled = false
    // You can update the global logger like this.
    log.GlobalLogger = l
    for j := 0; j < 100; j++ {
      wg.Add(1)
      go func() {
        for k := 0; k < 100; k++ {
          log.Debug("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
          log.Info("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
          log.Warn("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
          log.Error("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
        }
        wg.Done()
      }()
    }
    wg.Wait()
  }
}

func TestFile(t *testing.T) {
  l := log.New(log.OldStdout)
  // 1kb file size limit
  l.SetSizeLimit(1)
  l.SetOutputDir("log")

  // Writes 1kb of data
  for i := 0; i < 100; i++ {
    l.Info("I am a lot of text")
  }
  t.Fail()
}
