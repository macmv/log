package log

import (
  "os"
  "io"
  "fmt"
  "time"
  "bytes"
  "strings"
)

// This is the logger that is used globally. One is created
// at init(), and will capture os.Stdout. To change the
// settings, or make it log to a file, you can edit/replace
// log.GlobalLogger.
type Logger struct {
  // Log level. Can be any of the constants defined in level.go.
  // Defaults to INFO.
  Level Level

  // Header function. If this is non-nil, it is called any time a
  // line is created in the log. If it is nil, then the time and
  // [LEVEL]: are used as the header. Defaults to nil.
  HeaderFunc func(level Level) string

  // Is checked every time a message is logged. Set to false to
  // disable all colors. When true, colors will be printed to
  // out, but not to file_out. Defaults to true.
  ColorsEnabled bool

  // Colors that will be used whenever someone logs a message.
  // They are indexed with Level.
  colors []string

  // All logs are written to out.
  out io.Writer

  // File output directory.
  output_dir string
  // Limits for individual log files.
  file_size_limit int
  file_time_limit time.Duration
  // Current file size.
  file_size int
  // Current file we are writing to.
  file *os.File
}

// This is a logger interface. It is used with GlobalLogger,
// so that you may overwrite GlobalLogger with your own logger.
// This allows all of the static functions to call any code
// you specify.
type LeveledLogger interface {
  // Common logger functions. These
  // should all call Log(level, value...)
  Debug(value ...interface{})
  Info(value ...interface{})
  Warn(value ...interface{})
  Error(value ...interface{})

  // Logs a message.
  Log(level Level, value ...interface{})
}

// This creates a new logger. The logger will write
// everything to the given output. The default level
// is INFO, and it will print colors by default. To
// disable colors, set ColorsEnabled to false. To write
// to a file as well, call NewFile().
func New(out io.Writer) *Logger {
  l := Logger{}
  l.out = out
  l.Level = INFO
  l.ColorsEnabled = true
  l.colors = []string{
    "\033[34m", // DEBUG -> Blue
    "\033[0m",  // INFO  -> Reset (white/default color)
    "\033[33m", // WARN  -> Yellow
    "\033[31m", // ERROR -> Red
  }
  l.file_size_limit = 1024 * 10 // 10mb file limit
  l.file_time_limit = time.Hour // 1 file per hour
  return &l
}

// Logs a debug message. Colored blue by default.
func (l *Logger) Debug(value ...interface{}) { l.Log(DEBUG, value...) }
// Logs info. Colored white by default.
func (l *Logger) Info(value ...interface{}) { l.Log(INFO, value...) }
// Logs a warning. Colored yellow by default.
func (l *Logger) Warn(value ...interface{}) { l.Log(WARN, value...) }
// Logs an error. Colored red by default.
func (l *Logger) Error(value ...interface{}) { l.Log(ERROR, value...) }

// Logs a message. Level must be one of the constants defined
// in level.go. If you pass in level NONE, it will panic with
// index out of bounds.
func (l *Logger) Log(level Level, value ...interface{}) {
  if l == nil { return }
  if level < l.Level { return }
  var b bytes.Buffer
  if l.out != nil {
    b.Write([]byte(l.Color(level)))
    b.Write([]byte(l.Header(level)))
    b.Write([]byte(strings.TrimSpace(fmt.Sprint(value...))))
    b.Write([]byte{'\n'})
    l.out.Write(b.Bytes())
  }
  if l.output_dir != "" {
    b.Reset()
    // Don't write color here, as we don't want color
    // codes in files.
    b.Write([]byte(l.Header(level)))
    b.Write([]byte(strings.TrimSpace(fmt.Sprint(value...))))
    b.Write([]byte{'\n'})
    l.try_cycle_file(b.Len())
    l.file.Write(b.Bytes())
  }
}

// This will set one of the colors of the logger. The color
// string should be an ansi escape code. Level must be
// one of the constants defined in level.go. If you set
// the color of NONE, it will panic with index out of bounds.
func (l *Logger) SetColor(level Level, color string) {
  l.colors[level] = color
}

// Returns the color change ansi code
// to set the color to the given log
// level. If colors are disabled, this
// returns an empty string.
func (l *Logger) Color(level Level) string {
  if !l.ColorsEnabled { return "" }
  return l.colors[level]
}

// This gets the header for a log message.
// If l.HeaderFunc is non nil, this calls
// that and returns the result. Otherwise,
// it will return the date, time, and [L]:
// (the level's ShortString() with square
// brackets).
func (l *Logger) Header(level Level) string {
  if l.HeaderFunc != nil {
    return l.HeaderFunc(level)
  }
  // This server is being run in aws, so everything
  // is UTC. That is why there is no timezone here.
  now := time.Now()
  return fmt.Sprintf(
    "%04d-%02d-%02d %02d:%02d:%02d [%v]: ",
    now.Year(),
    now.Month(),
    now.Day(),
    now.Hour(),
    now.Minute(),
    now.Second(),
    level.ShortString(),
  )
}
