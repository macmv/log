# A simple logger for Go

This logger package aims to do one thing: add the basics of logging that the builtin package does not do.
Currently, it captures everything going to stdout, and prints it as info. It has four levels, each with
colors that can be changed/enabled/disabled. It can also log to a file as well as stdout. For my needs,
this is all I want a logger to do; I just can't work with a single Println function for everything.

## Future Development

I am open to new features being added, but I don't have anything planned. The main thing I want to be
sure of is that it stays backwards compatible. I don't want to break someone's project because I added
a shiny new feature.

The one thing I may add in the future is formatted versions of all of the log messages. That is, an
`Infof` function instead of just `Info`. I don't have any plan on when/if this will be added, but it
is the one thing I am considering. An issue with a strong argument will probably get it completed in
a few days.

## Contributing

Send a PR. If you don't like my formatting, then just use a git hook to reformat all the tabs with 2 spaces.
