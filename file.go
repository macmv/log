package log

import (
  "os"
  "fmt"
  "time"
  "errors"
  "compress/gzip"
  "path/filepath"
)

// Sets the directory to output logs to.
func (l *Logger) SetOutputDir(dir string) {
  l.output_dir = dir
}

// Sets the size limit of logs files in kilobytes.
// If files reach this size before the time limit,
// the are compressed and saved, and the loger starts
// writing to a file name <name>{.0, .1, .2}.log etc.
func (l *Logger) SetSizeLimit(kb int) {
  l.file_size_limit = kb
  if l.file == nil {
    l.cycle_file()
  }
}

// Sets the time limit. This time limit is used to
// determine the file name format, and is also
// used to determine when to start outputting
// a new file. The time limit only makes sense if
// it is one of time.Second, time.Minute, time.Hour,
// or a multiple of time.Hour. This is because files
// are cycled every dur after midnight. So if you
// pass a duration that does not evenly fit into
// 24 hours, you will get a file that is shorter once
// per day. Other than that, any time limit will work
// for this function.
func (l *Logger) SetTimeLimit(dur time.Duration) {
  l.file_time_limit = dur
}

// Called every time a message is logged. This makes
// sure any log file will not go above file_size_limit,
// and that files get cycled after the gived time limit.
// message_length is used to make sure no file gets
// longer than file_size_limit; this should be called
// before writing to the current file, but after
// generating the log message.
func (l *Logger) try_cycle_file(message_length int) {
  if l.file_size + message_length > l.file_size_limit * 1024 {
    l.file_size = 0
    l.cycle_file()
  }
  l.file_size += message_length
}

// Cycles the filename, given the current state of the
// logger. This checks for files on disk, so it should
// only be called when the logger knows it should cycle
// filenames. This also compresses the previous file.
func (l *Logger) cycle_file() {
  now := time.Now()
  new_filename := filepath.Join(l.output_dir, fmt.Sprintf(
    "%04d-%02d-%02d",
    now.Year(),
    now.Month(),
    now.Day(),
  ))
  if l.file_time_limit < 24 * time.Hour {
    new_filename += fmt.Sprintf("-%02d:%02d", now.Hour(), now.Minute())
  }
  if l.file_time_limit < time.Minute {
    new_filename += fmt.Sprintf(":%02d", now.Second())
  }
  new_filename += ".log"
  _, err := os.Stat(l.output_dir)
  if errors.Is(err, os.ErrNotExist) {
    err := os.MkdirAll(l.output_dir, 0755)
    if err != nil {
      // Cannot output to files, don't try again
      l.output_dir = ""
      l.Error("during cycle: error while creating log directory: ", err)
    }
  }
  num := 0
  for {
    filename := new_filename
    if num != 0 {
      filename += fmt.Sprintf(".%d", num)
    }
    _, err := os.Stat(filename)
    if errors.Is(err, os.ErrNotExist) {
      // Past files should have been gzipped. Need to check those as well.
      _, err = os.Stat(filename + ".gz")
      if errors.Is(err, os.ErrNotExist) {
        new_filename = filename
        break
      } else if err != nil {
        // File writing is broken, don't write files in the future.
        // The only reason os.Open() would fail is a PathError, so
        // we know opening won't work in the future.
        l.output_dir = ""
        l.Error("during cycle: error while getting stat of log file: ", err)
        break
      }
    } else if err != nil {
      // File writing is broken, don't write files in the future.
      // The only reason os.Open() would fail is a PathError, so
      // we know opening won't work in the future.
      l.output_dir = ""
      l.Error("during cycle: error while getting stat of log file: ", err)
      break
    }
    num++
  }
  // Means we already had a file open
  if l.file != nil {
    l.compress()
  }
  l.file, err = os.Create(new_filename)
  if err != nil {
    // We just checked for any errors, so we want to fail
    // if err is non nil.
    l.output_dir = ""
    l.Error("during cycle: error while creating log file: ", err)
  }
}

// Compresses l.file. This will move it to
// <filename>.tmp, then write the compressed
// version to <filename>.gz, then delete
// <filename>.tmp. This means there is no
// way you lose logs, even if the program is
// killed.
func (l *Logger) compress() {
  l.file.Close()

  old_filename := l.file.Name()
  tmp_filename := l.file.Name() + ".tmp"
  new_filename := l.file.Name() + ".gz"

  err := os.Rename(old_filename, tmp_filename)
  if err != nil { l.Error("during compression: error renaming log file: ", err); return }
  // Open the tmp file for reading
  tmp, err := os.Open(tmp_filename)
  if err != nil { l.Error("during compression: error reading tmp file: ", err); return }
  // Make sure to truncate the new file, if it exists
  file, err := os.Create(new_filename)
  if err != nil { l.Error("during compression: error creating new file: ", err); return }
  w := gzip.NewWriter(file)
  // Compress in 1kb chunks
  buf := make([]byte, 1024)
  for {
    n, err := tmp.Read(buf)
    if err != nil { l.Error("during compression: error reading from tmp: ", err); return }
    _, err = w.Write(buf[:n])
    if err != nil { l.Error("during compression: error writing to gzip: ", err); return }
    if n < 1024 {
      break
    }
  }
  err = w.Close()
  if err != nil { l.Error("during compression: error closing gzip: ", err); return }
  err = tmp.Close()
  if err != nil { l.Error("during compression: error closing tmp file: ", err); return }
  err = file.Close()
  if err != nil { l.Error("during compression: error closing log file: ", err); return }
  err = os.Remove(tmp_filename)
  if err != nil { l.Error("during compression: error removing tmp file: ", err); return }
}

// Closes the logger. This will make sure to gzip the
// most recent file. A ctrl-c handler is added in
// order to close the global logger.
func (l *Logger) Close() {
  if l.file != nil {
    l.compress()
  }
}
